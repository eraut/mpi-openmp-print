#include <mpi.h>
#include <omp.h>
#include <stdio.h>

#include <unistd.h>
#include <limits.h>

int main(void)
{
  MPI_Init(NULL,NULL);

  int world_size;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  char name[HOST_NAME_MAX+1];
  gethostname(name, sizeof(name));

  for (int r = 0; r < world_size; r++) {
    if (world_rank == r) {
#pragma omp parallel
      {
        int thread_num = omp_get_thread_num();
        int num_threads = omp_get_num_threads();
        for (int t = 0; t < num_threads; t++) {
          if (thread_num == t) {
            printf("%s: rank %d, thread %d\n", name, world_rank, thread_num);
          }
#pragma omp barrier
        }
      }
    }
    MPI_Barrier(MPI_COMM_WORLD);
  }
  MPI_Finalize();
}
